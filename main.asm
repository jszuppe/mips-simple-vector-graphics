# 
# Jakub Szuppe <j.szuppe at gmail dot com>
# 11.2012

# MIPS asembler
#
# "Grafika Wektorowa"
#
# Wczytuje dane z pliku data.txt i przetwarza na plik *.bmp
# data.txt moze zawierac opisy ko� i prostok�t�w oddzielone spacj�
# Ko�o: K(x, y, promien, kolor)
# Prostok�t: P(x, y, szerokosc, wysokosc, kolor)

                .data

IntroTxt:	.asciiz "Program 'Grafika Wektorowa', Jakub Szuppe 11.2012\n"
DoneTxt:	.asciiz "Wykonano bez bledow."

DataBuffer:	.space	121
_dataBufferSize:.word	121


DataFile:       .asciiz "data.txt"
BmpFile:        .asciiz "result.bmp"
EmptyBmp:	.asciiz "empty.bmp"

Header:         .space  54 
Pixels:         .word	0

_pixelsSize:	.word 	0
_headerSize:	.word 	54

_bmpWidth:	.word	0
_bmpHeight:	.word	0
_bmpPadding:	.word	0
                .text
                .globl  main
                
# ############################################################################# 
# main
#
# Tutaj wszystko sie dzieje!
#
main:		
		la 	$a0, IntroTxt
		li 	$v0, 4		
		syscall	

		# **********  WCZYTANIE DANYCH Z EmptyBmp *******
		# Otwieram Empty, zeby miec nag�owek i biale tlo
		la	$a0, EmptyBmp
   		li	$a1, 0
   		li	$a2, 0
   		li	$v0, 13
   		syscall   		
   		move	$s0, $v0
   		
   		# Zczytuje nag�owek
   		move	$a0, $s0
		la	$a1, Header
		lw 	$a2, _headerSize
		li	$v0, 14
		syscall
		
		# ***************  ANALIZA NAGLOWKA ************			
		# t4 - szerokosc, t5 - wysokosc, t6 - padding	
			
		la	$t3, Header		
		addiu	$t3, $t3, 18		
		lw	$t4, ($t3)		
		sw	$t4, _bmpWidth	
		
		addiu	$t3, $t3, 4
		lw	$t5, ($t3)
		sw	$t5, _bmpHeight
		
		# Obliczam padding
		
		li	$t6, 3
		and	$t6, $t4, $t6	
		sw	$t6, _bmpPadding
		
		addiu	$t3, $t3, 12
		lw	$t4, ($t3)
		sw	$t4, _pixelsSize
		
		# t4 - szerokosc, t5 - wysokosc, t6 - padding		
		
		move	$a0, $t4	# Alokacja pamieci i zapis adresuu pod label Pixels
		li	$v0, 9			
		syscall		
		sw	$v0, Pixels					
																				
		# *************** KONIEC ANALIZY NAGLOWKA *******
						
		#Zczytuje biale t�o (puste)
		move	$a0, $s0			
		lw	$a1, Pixels				
		lw	$a2, _pixelsSize
		li	$v0, 14			
		syscall
   		#Zamykam
   		move	$a0, $s0
		li	$v0, 16
		syscall
		
   		# **********  DANE Z EmptyBmp WCZYTANE **********
data:  		
   		# ********************************************************************************************
   		#
   		#					OPERACJE NA DATA.TXT 
   		#
   		
   		
   		# Potrzebuje okreslisc jaka partie danych wczytuje.
   		# P�niej analizuje wczytany teskt, jak znajde K() lub P(), to czytam arguemnty i rozszyfrowuje je z kod�w ascii.
   		# Pozycje, wymiary <-- zapisane jako liczby dziesietne; kolor <--- heksadecymalnie
   		# np. "P(10, 11, 20, 21, FF0000)" -> rysuje Prostok�t x=10, y=11, width=20, height=21, kolor=FF0000 (00FF0000)
   		# Potrzeba funkcji string->int string->hex
 		 
   		li	$v0, 13
   		la	$a0, DataFile
   		li	$a1, 0
   		li	$a2, 0  		
   		syscall   	
   			
   		move	$s0, $v0
   		move 	$s1, $zero
   		blt	$s0, $zero, data_exit
   		b	data_load   		
data_buff_repair:
		# Ta funkcja naprawia buffor 
		move	$t0, $s1	#licznik
		la	$t1, DataBuffer 
data_buff_repair_loop:		

		lbu	$t3, ($s2)
		addiu	$s2, $s2, 1
		sb	$t3, ($t1)
		addiu	$t1, $t1, 1
		subiu	$t0, $t0, 1
		bnez 	$t0, data_buff_repair_loop	
data_load:  		
   		# Zczytuje do buffora X znakow
   		# s0 - PLIK
   		# s1 - liczba elementow ktore pozostaly do przeczytania
   		# s2 - wskaznik
   		move	$a0, $s0
		la	$a1, DataBuffer
		addu	$a1, $a1, $s1
		lw 	$a2, _dataBufferSize
		subu	$a2, $a2, $s1
		li	$v0, 14
		syscall			
		
   		addu	$s1, $s1, $v0
   		beq	$s1, $zero, data_exit
   		
   		la	$s2, DataBuffer
data_decide: 		
   		beq	$s1, $zero, data_load
   		lbu	$t1, ($s2)	   			
   		beq   	$t1, 'P', data_rectangle
   		beq   	$t1, 'K', data_circle
   		addiu	$s2, $s2, 1
   		subiu	$s1, $s1, 1
   		b	data_decide
   		#bltu 	$t1, ' ', data_exit					
data_circle:	
		bltu	$s1, 24, data_buff_repair
		addiu	$s2, $s2, 2
		move	$a0, $s2
		jal	str_to_int		
		move	$s3, $a0
		
		addiu	$s2, $s2, 5
		move	$a0, $s2
		jal	str_to_int		
		move	$s4, $a0
		
		addiu	$s2, $s2, 5
		move	$a0, $s2
		jal	str_to_int		
		move	$s5, $a0
		
		addiu	$s2, $s2, 5
		move	$a0, $s2
		jal	str6_to_hex		
		move	$s6, $a0	
				
		move	$a0, $s3
		move	$a1, $s4
		move	$a2, $s5	
		move	$a3, $s6
		jal	draw_circle	
		
		addiu	$s2, $s2, 7
		subiu	$s1, $s1, 24		
			
		b	data_decide				
data_rectangle:					
   		bltu	$s1, 29, data_buff_repair
   		addiu	$s2, $s2, 2
		move	$a0, $s2
		jal	str_to_int		
		move	$s3, $a0
		
		addiu	$s2, $s2, 5
		move	$a0, $s2
		jal	str_to_int		
		move	$s4, $a0
		
		addiu	$s2, $s2, 5
		move	$a0, $s2
		jal	str_to_int		
		move	$s5, $a0
		
		addiu	$s2, $s2, 5
		move	$a0, $s2
		jal	str_to_int		
		move	$s6, $a0
		
		addiu	$s2, $s2, 5
		move	$a0, $s2
		jal	str6_to_hex		
		move	$s7, $a0
		
		move	$a0, $s3
		move	$a1, $s4
		move	$a2, $s5	
		move	$a3, $s6		
		addiu	$sp, $sp, -4
		sw	$s7, ($sp)
		
		jal	draw_rectangle	
		
		addiu	$s2, $s2, 7
		subiu	$s1, $s1, 29		
			
		b	data_decide
data_exit:		
   		# Zamykam DataFile
   		move	$a0, $s0
		li	$v0, 16
		syscall
   		
   		#
   		#					KONIEC OPERACJI NA DATA.TXT
   		#
   		# ********************************************************************************************
   		
   		# ********************************************************************************************
   		#
   		#				TUTAJ WBUDOWANE W KOD OPERACJE NA PIKSELACH
   		#
   	
		#
   		#						KONIEC
   		#
   		# ********************************************************************************************
   		   		      		   
   		# **********************************************
		#ZAPIS Otwieram plik wynikowy do edycyji
		la	$a0, BmpFile
   		li	$a1, 1
   		li	$a2, 0
   		li	$v0, 13
   		syscall    		
   		move	$s0, $v0   		   		   		
   		   		   		   				   				   		
   		#Zapis nag��wka do pliku wynikowego.bmp
   		li	$v0, 15
		move	$a0, $s0
		la	$a1, Header
		lw	$a2, _headerSize
		syscall
		
		#Zapis pikseli do pliku wynikowego.bmp
		li	$v0, 15
		move	$a0, $s0
		lw	$a1, Pixels			
		lw	$a2, _pixelsSize
		syscall
		
		#Zamykam plik wyniowy
		move	$a0, $s0
		li	$v0, 16
		syscall
		# ********************************************** 
		
main_exit:		
		# **********************************************
		#Koniec programu
		
		#Drukuje, ze program sie wykonal poprawnie
		la 	$a0, DoneTxt
		li 	$v0, 4		
		syscall
		
		#Koniec
		li	$v0, 10
		syscall
		
		# **********************************************
		# *************   MAIN END   *******************
		# **********************************************

# ############################################################################# 
# str6_to_hex
#
# Funkcja zamienia string w zapisie heksadecymanym na liczbe i zwraca ja w a0.
# Funkcja nie jest uniwersalna. Stworzona jest z mysla o zamianie coloru np (#FFAACC) na liczbe. 
# 
# Argumenty	a0 = adres 1 znaku;
# Stale		dlugosc ciagu = 6 
# Zwraca	a0 = wartosci 
str6_to_hex:		
	
		#move	$t0, $a0	# adres znaku
		move 	$t1, $zero	# licznik petli
		move	$t2, $zero	# zaladowany bajt znaku
		move	$t3, $zero	# akumulator	
str6_to_hex_out:
			
		sll	$t3, $t3, 4
		addu	$t3, $t3, $t2 
		bgeu 	$t1, 6, str6_to_hex_exit
str6_to_hex_in:
		
		lbu	$t2, ($a0)		# wczytuje pierwszy znak
		addiu	$a0, $a0, 1		# kolejny znak
		addiu	$t1, $t1, 1		# licznik petli += 1
		
		subiu	$t2, $t2, '0'		# ze znaku uzyskuje liczbe				
		bleu 	$t2, 9, str6_to_hex_out	# jezeli 0-9 to dobrze i skaczemy
		subiu	$t2, $t2, 7 		# jezeli A-F to poprawiam. 'A' - '9' - 1 = 7
		b 	str6_to_hex_out
str6_to_hex_exit:
		
		move	$a0, $t3
		jr 	$ra
		
# ############################################################################# 
# str_to_int
# 
#
# Argumenty	a0 = adres pierwszego znaku 1 ciagu;
# Stale		dlugosc kazdego ciagu = 4
# Zwraca	a0 = wartosc int1; 
str_to_int:
		
		move	$t0, $zero		# int1
		
str_to_int_loop:					
		lbu	$t5, ($a0)		
		addiu	$a0, $a0, 1
		subiu	$t5, $t5, '0'
		mulu	$t5, $t5, 1000
		addu	$t0, $t0, $t5
		
		lbu	$t5, ($a0)		
		addiu	$a0, $a0, 1
		subiu	$t5, $t5, '0'
		mulu	$t5, $t5, 100
		addu	$t0, $t0, $t5
		
		lbu	$t5, ($a0)		
		addiu	$a0, $a0, 1
		subiu	$t5, $t5, '0'
		mulu	$t5, $t5, 10
		addu	$t0, $t0, $t5
		
		lbu	$t5, ($a0)	
		subiu	$t5, $t5, '0'			
		addu	$t0, $t0, $t5
		
		move	$a0, $t0		
		
		jr	$ra
				
# ############################################################################# 
# set_pixel
#
# Funkcja zamalowywuje podany piksel na podanych kolor (zapis 0xFFFFFF);
# 
# Napisana tylko w celu opanowania tego jak dziala bitmapa itd itp
# 
#
# Argumenty	a0 = x;  a1 = y;  a2 = kolor; 
# Zwraca	nic
set_pixel:
		# Pobieram wymiary
		lw 	$t0, _bmpWidth		
		lw 	$t1, _bmpHeight	
		
		# Sprawdzam zakres
		bltz	$a0, set_pixel_exit		# x < 0		
		bltz	$a1, set_pixel_exit		# y < 0		
		bge	$a0, $t0, set_pixel_exit	# x >= szerokosc bmp		
		bge	$a1, $t1, set_pixel_exit	# y >= wysokosc bmp		
					
		# Licze piksel
		
		lbu	$t2, _bmpPadding
		multu	$t2, $a1
		mflo	$t2
		
		multu	$a1, $t0			# y * szerokosc bitmapy			
		mflo	$a1				# zwracam wynik
		addu	$a0, $a0, $a1			# dodaje do x
		
		li	$t8, 3
		multu 	$a0, $t8
		mflo	$a0
		
		addu	$a0, $a0, $t2
		
		# 
		lw	$t0, Pixels			# Poczatek 
		addu	$a0, $a0, $t0			# Dany pliksel	
			
		# Jade po R, G, B i zapisuje					  
   		sb	$a2, ($a0)	#B		
   		sra	$a2, $a2, 8
   		addiu	$a0, $a0, 1
   		
   		sb	$a2, ($a0)	#G		
   		sra	$a2, $a2, 8
   		addiu	$a0, $a0, 1
   			
   		sb	$a2, ($a0)	#R	
set_pixel_exit:
		jr 	$ra
# ############################################################################# 
# draw_hline
#
# Rysuje pozioma linie. Potrzebna przy rysowaniu kola.
# Argumenty	a0 = x;  a1 = y;  a2 = dlugosc;  a3 = kolor;
# Zwraca	nic
draw_hline:		
		
		# Zapamietuje kolory w $t5, $t6, $t7 (R, G, B)	
		# W $t7 dwa najmniej znaczace bajty to B, w $t6 - G, $t5 - R  
		# 	
		move	$t7, $a3	
   		sra 	$t6, $t7, 8 
   		sra	$t5, $t6, 8   
   		
   		# Pobieram wymiary				
		lw 	$t0, _bmpWidth		
		lw 	$t1, _bmpHeight	
   		
		# Jezeli poczatek prostokata jest poza obrazkiem to wychodze
		bge	$a0, $t0, draw_hline_exit	# X
		bge	$a1, $t1, draw_hline_exit	# Y
		
		# Do szerokosci i wysokosci dodaje odpowiednio x i y ---> dostaje x2 
		add	$a2, $a2, $a0 # X2
		
		# Jezeli x2 jest mniejsza od zera
		bltz	$a2, draw_hline_exit	# X2
				
		# Ustawiam poprawne X
		bgez	$a0, draw_hline_X_OK
		move	$a0, $zero	# Jezeli X < 0 ---> X := 0
				
draw_hline_X_OK:
		# Ustawiam poprawne Y
		bgez	$a1, draw_hline_Y_OK
		move	$a1, $zero	# Jezeli Y < 0 ---> Y := 0		
		
draw_hline_Y_OK:
		# Ustawiam poprawne X2
		bltu   	$a2, $t0, draw_hline_X2_OK
		move	$a2, $t0	# Jezeli X > _bmpWidth ---> X2 := _bmpWidth		
		
draw_hline_X2_OK:
		
		# t0 - szerokosc
		# t4 - PIKSEL (adres piksela)
		# t5 - R
		# t6 - G
		# t7 - B	
		
		# Licze kra�ce
		li	$t2, 3
		multu 	$t0, $t2
		mflo	$t0	# Szerokosc * 3 
		
		lw	$t8, _bmpPadding
		addu	$t0, $t0, $t8	# Dodaje do liczb bajtow w wierszu puste bajty wyrownujace do 4
		
		multu 	$a0, $t2
		mflo	$a0	# X1 * 3 
		multu 	$a2, $t2
		mflo	$a2	# X2 * 3
		multu	$a1, $t0
		mflo	$a1	# Y * Szerokosc * 3
						
		lw	$t4, Pixels			# Adres bazowy pikseli		
		add	$t4, $t4, $a1			# adres piksela += y
		add	$t4, $t4, $a0			# adres piksela += x1
				
draw_hline_loop:
		# Rysuje piksel	(stale x1, y1, staja sie zmiennymi x, y)
				
		sb	$t7, ($t4)	#B   		
   		addiu	$t4, $t4, 1   		
   		sb	$t6, ($t4)	#G   		
   		addiu	$t4, $t4, 1   			
   		sb	$t5, ($t4)	#R
   		addiu	$t4, $t4, 1   			
		
		addiu	$a0, $a0, 3				# x += 3
		bltu	$a0, $a2, draw_hline_loop		# Nie doszlismy do konca (x<x2)
								
draw_hline_exit:
		
		jr $ra	
# ############################################################################# 
# draw_rectangle
#
# Rysuje prostokat wypelniony kolorem
# Argumenty	a0 = x;  a1 = y;  a2 = szerokosc prostakata;  a3 = wysokosc prostokata;  stos: kolor;
# Zwraca	nic
draw_rectangle:		
		
		# Zapamietuje kolory w $t5, $t6, $t7 (R, G, B)	
		# W $t7 dwa najmniej znaczace bajty to B, w $t6 - G, $t5 - R  
		# 	
		lw	$t7, ($sp)			# Zabieram kolor ze stosu
		addiu	$sp, $sp, 4	
   		sra 	$t6, $t7, 8 
   		sra	$t5, $t6, 8   
   		
   		# Pobieram wymiary				
		lw 	$t0, _bmpWidth		
		lw 	$t1, _bmpHeight	
   		
		# Jezeli poczatek prostokata jest poza obrazkiem to wychodze
		bge	$a0, $t0, draw_rectangle_exit	# X1
		bge	$a1, $t1, draw_rectangle_exit	# Y1
		
		# Do szerokosci i wysokosci dodaje odpowiednio x i y ---> dostaje x2, y2 
		add	$a2, $a2, $a0 # X2
		add	$a3, $a3, $a1 # Y2
		
		# Jezeli x2 lyb y2 jest mniejsza od zera
		bltz	$a2, draw_rectangle_exit	# X2
		bltz	$a3, draw_rectangle_exit	# Y2
		
		
		# Ustawiam poprawne X1
		bgez	$a0, draw_rectangle_X1_OK
		move	$a0, $zero	# Jezeli X1 < 0 ---> X1 := 0
				
draw_rectangle_X1_OK:
		# Ustawiam poprawne Y1
		bgez	$a1, draw_rectangle_Y1_OK
		move	$a1, $zero	# Jezeli Y1 < 0 ---> Y1 := 0		
		
draw_rectangle_Y1_OK:
		# Ustawiam poprawne X2
		bleu 	$a2, $t0, draw_rectangle_X2_OK
		move	$a2, $t0	# Jezeli X2 > _bmpWidth ---> X := _bmpWidth
		
draw_rectangle_X2_OK:
		# Ustawiam poprawne Y2
		bleu	$a3, $t1, draw_rectangle_Y2_OK
		move	$a3, $t1	# Jezeli Y2 > _bmpHeight ---> Y2 := _bmpHeight
		
draw_rectangle_Y2_OK:
		# X, Y i X2, Y2 ustawione poprawnie
		
		# t0 - szerokosc
		# t1 - wysokosc
		# t2 - adres Pixels
		# t3 - X1
		# t4 - PIKSEL (adres piksela)
		# t5 - R
		# t6 - G
		# t7 - B	
					
		# Licze kra�ce
		li	$t2, 3
		multu 	$t0, $t2
		mflo	$t0	# Szerokosc * 3 
		
		lw	$t8, _bmpPadding
		addu	$t0, $t0, $t8	# Dodaje do liczb bajtow w wierszu puste bajty wyrownujace do 4
		
		multu 	$t1, $t2
		mflo	$t1	# Wysokosc * 3
		multu 	$a0, $t2
		mflo	$a0	# X1 * 3 
		multu 	$a2, $t2
		mflo	$a2	# X2 * 3
		multu	$a1, $t0
		mflo	$a1	# Y1 * Szerokosc * 3
		multu	$a3, $t0
		mflo	$a3	# Y2 * Szerokosc * 3
		
		move	$t3, $a0 # Zapisuje X1
		
		lw	$t2, Pixels		# Adres bazowy pikseli
				
draw_rectangle_loop:
		# Rysuje piksel	(stale x1, y1, staja sie zmiennymi x, y)
		add	$t4, $t2, $a0				# adres piksela += Pixels + x
		add	$t4, $t4, $a1				# adres piksela += y
		
		sb	$t7, ($t4)	#B   		
   		addiu	$t4, $t4, 1   		
   		sb	$t6, ($t4)	#G   		
   		addiu	$t4, $t4, 1   			
   		sb	$t5, ($t4)	#R		
		
		addiu	$a0, $a0, 3				# x += 3
		bltu	$a0, $a2, draw_rectangle_loop		# Nie doszlismy do konca (x<x2)
		
		
		addu	$a1, $a1, $t0				# y += 3*szerokosc bmp (nastepny wiersz)
		move	$a0, $t3				# x = x1 (zeby zacz�c od lewej strony prostokata)
		bltu 	$a1, $a3, draw_rectangle_loop		# Nie ostatni wiersz, to dalej rysujemy. (y<y2)
		
				
draw_rectangle_exit:
		
		jr $ra	
					
# ############################################################################# 
# draw_circle
#
# Rysuje okrag wypelniony kolorem
#
# Rysujemy z pomoc� algorytmu Bresenhama, wersja ciut zoptymalizowana (przynajmniej wed�ug wiki) 
# Dlatego, bo to nie trudne, a sensowne.
# Link do �r�d�a: http://en.wikipedia.org/wiki/Midpoint_circle_algorithm#Optimization
#
#
# Argumenty	a0 = cx;  a1 = cy;  a2 = r;  a3 = kolor;
# Zwraca	nic, ale za to rysuje!
draw_circle:
		# Okaza�o si�, �e potrzebuje rejestrow s, zatem zapisuje na stosie to co w nich bylo.
		addiu	$sp, $sp, -28
		sw	$ra, 24($sp)
		sw	$s0, 20($sp)
		sw	$s1, 16($sp)
		sw	$s2, 12($sp)
		sw	$s3, 8($sp)
		sw	$s4, 4($sp)
		sw	$s5, ($sp)

		
		move	$s0, $a0		# s0 = cx (circle x)
		move	$s1, $a1		# s1 = cy	
		subu	$s2, $zero, $a2		# s2 = b��d = -r	
		move	$s3, $a3		# s3 = kolor		
		move	$s4, $a2		# s4 = x = r		
		li	$s5, 0			# y = 0
		
		
draw_circle_loop:
		# Warunek ko�ca
		bltu $s4, $s5 draw_circle_exit	# x >= y to koniec
		
		# Rysuje 8 pikseli 
		# 
		# Przerobie p�niej na rysowanie linii przez co okr�g b�dzie wype�niony.
		# B�d� wtedy rysowane cztery linie.
		# 
		
			#1
			#addu	$a0, $s0, $s4		# cx + x
			#addu	$a1, $s1, $s5		# cy + y
			#move	$a2, $s3		# kolor
			#jal	set_pixel
			#2
			#subu	$a0, $s0, $s4		# cx - x
			#addu	$a1, $s1, $s5		# cy + y
			#move	$a2, $s3		# kolor
			#jal	set_pixel		
		# 1 i 2
		subu	$a0, $s0, $s4		# cx - x
		addu	$a1, $s1, $s5		# cy + y
		addu	$a2, $s4, $s4		# 2x
		move	$a3, $s3
		jal 	draw_hline		
			#3
			#addu	$a0, $s0, $s4		# cx + x
			#subu	$a1, $s1, $s5		# cy - y
			#move	$a2, $s3		# kolor
			#jal	set_pixel
			#4
			#subu	$a0, $s0, $s4		# cx - x
			#subu	$a1, $s1, $s5		# cy - y
			#move	$a2, $s3		# kolor
			#jal	set_pixel
		# 3 i 4
		subu	$a0, $s0, $s4		# cx - x
		subu	$a1, $s1, $s5		# cy - y
		addu	$a2, $s4, $s4		# 2x
		move	$a3, $s3
		jal 	draw_hline	
			#5
			#addu	$a0, $s0, $s5		# cx + y
			#addu	$a1, $s1, $s4		# cy + x
			#move	$a2, $s3		# kolor
			#jal	set_pixel
			#6
			#subu	$a0, $s0, $s5		# cx - y
			#addu	$a1, $s1, $s4		# cy + x
			#move	$a2, $s3		# kolor
			#jal	set_pixel
		# 5 i 6
		subu	$a0, $s0, $s5		# cx - y
		addu	$a1, $s1, $s4		# cy + x		
		addu	$a2, $s5, $s5		# 2y	
		move	$a3, $s3
		jal 	draw_hline	
			#7
			#addu	$a0, $s0, $s5		# cx + y
			#subu	$a1, $s1, $s4		# cy - x
			#move	$a2, $s3		# kolor
			#jal	set_pixel
			#8
			#subu	$a0, $s0, $s5		# cx - y
			#subu	$a1, $s1, $s4		# cy - x
			#move	$a2, $s3		# kolor
			#jal	set_pixel
		# 7 i 8
		subu	$a0, $s0, $s5		# cx - y
		subu	$a1, $s1, $s4		# cy - x
		addu	$a2, $s5, $s5		# 2y	
		move	$a3, $s3
		jal 	draw_hline			
		#
		
		addu	$s2, $s2, $s5		# b��d += y
		addiu	$s5, $s5, 1		# y += 1
		addu	$s2, $s2, $s5		# b��d += y

		bltz	$s2, draw_circle_loop	# Jezeli b��d < 0 to p�tla, je�eli b��d >= 0 to obliczamy:
		
		subu	$s2, $s2, $s4		# b��d -= x
		subiu	$s4, $s4, 1		# x -= 1
		sub	$s2, $s2, $s4		# b��d -= x
		
		b	draw_circle_loop	# p�tla
		
		
draw_circle_exit:
		# Wracam do rejestrow s ich oryginalne wartosci
		lw	$s5, ($sp)
		lw	$s4, 4($sp)
		lw	$s3, 8($sp)
		lw	$s2, 12($sp)
		lw	$s1, 16($sp)
		lw	$s0, 20($sp)
		lw	$ra, 24($sp)
		addiu	$sp, $sp, 28

		jr	$ra
				
###########################################################################################
